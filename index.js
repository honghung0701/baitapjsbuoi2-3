//Tính tiền lương
function tinhTien() {
    var luong1NgayValue = document.getElementById('luong1Ngay').value;
    var soNgayLamValue = document.getElementById ('soNgayLam').value;
    var luongNhanVien = luong1NgayValue * soNgayLamValue;
    document.getElementById('tinhLuong').innerHTML =`${luongNhanVien} VNĐ` ;
}
//Tính giá trị trung bình
function trungBinh() {
    var thu1 = document.getElementById('soThuNhat').value;
    var thu2 = document.getElementById('soThuHai').value;
    var thu3 = document.getElementById('soThuBa').value;
    var thu4 = document.getElementById('soThuTu').value;
    var thu5 = document.getElementById('soThuNam').value;
    var trungBinhNamSo = 0;
    trungBinhNamSo = (thu1*1 + thu2*1 + thu3*1 + thu4*1 + thu5*1)/5;
    document.getElementById('trungbinh').innerHTML = trungBinhNamSo;
}
//Quy đổi tiền
function quyDoi() {
    const USD = 23500;
    var tienCanDoi = document.getElementById('soTien').value;
    var tienDaDoi = USD * tienCanDoi;
    document.getElementById('tienDaDoi').innerHTML = `${tienDaDoi} VNĐ` ;
}
//Tính diện tích, chu vi hình chữ nhật
function tinhDientichChuvi() {
    var chieuDaiValue = document.getElementById('chieuDai').value;
    var chieuRongValue = document.getElementById('chieuRong').value;
    var chuVi = (chieuDaiValue*1 + chieuRongValue*1)*2;
    var dienTich = chieuDaiValue * chieuRongValue;
    document.getElementById('ketQua').innerHTML = `
    Chu vi: ${chuVi};
    Diện tích: ${dienTich};
    `
}
//Tính tổng 2 ký số
function tinhTong() {
    var number = document.getElementById('number').value;
    var tong = Math.floor(number/10) + number%10;
    document.getElementById('tong2KySo').innerHTML = tong; 
}